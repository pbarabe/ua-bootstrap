const backstop = require('backstopjs')
const backstopConfig = require('../backstop.config.js')

module.exports = {
  /* Generates the reference images used for Backstop's tests */
  reference: (gulp, config) => () => {
    return backstop('reference', { config: backstopConfig() })
  },

  /* Run Backstop tests against the stored reference images */
  run: (gulp, config) => (cb) => {
    config.bs.emitter.on('service:exit', cb)
    config.bs.init(Object.assign({}, config.bsConfig, { open: false }), () => {
      backstop('test', {
        config: backstopConfig({ testHost: 'http://localhost:' + config.bs.getOption('port') })
      }).then(config.bs.exit, config.bs.exit)
    })
  },

  /* Runs Backstop in the context of the BitBucket pipeline */
  pipeline: (gulp) => () => {
    const config = backstopConfig({
      refHost: `${process.env.S3URL}/ua-bootstrap/master`,
      testHost: `${process.env.S3URL}/ua-bootstrap/${process.env.BRANCH}`
    })

    return backstop('reference', { config }).then(() => {
      return backstop('test', { config })
    })
  }
}
