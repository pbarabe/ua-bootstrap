<h1 id="callouts">Callouts</h1>

<h3>Usage</h3>
<p>Callouts can be used similarly to label classes, or with any brand color.</p>
<div class="example" data-example-id="callouts-default">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="callout callout-red">
    Here is text inside of an Arizona Red callout
  </div>
</div>

```html
<div class="callout callout-red"></div>
```

<h3>Headings</h3>
<p>If you want your callout to include a heading, you can use an h4, and it will be styled with the callout.</p>
<div class="example" data-example-id="callouts-heading">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="callout callout-blue">
    <h4>I am a callout heading.</h4>
    <p>I am a callout body.</p>
  </div>
  <div class="callout callout-warning">
    <h4>I am a warning heading.</h4>
    <p>I am a warning body.</p>
  </div>
</div>

```html
<div class="callout callout-blue">
  <h4>I am a callout heading.</h4>
  <p>I am a callout body.</p>
</div>
<div class="callout callout-warning">
  <h4>I am a warning heading.</h4>
  <p>I am a warning body.</p>
</div>
```

<h3>Optional classes</h3>
<p>Add a darker background to your callout with ```.callout-dark```</p>
<div class="callout callout-warning">
  <h4>Accessibility Warning</h4>
  <p>Not all colors pass accessibility tests when put in a callout with a darkened background.</p>
</div>
<div class="example" data-example-id="callouts-optional">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="callout callout-azurite callout-dark">
    Here is text inside a callout with a dark background
  </div>
</div>

```html
<div class="callout callout-azurite callout-dark">...</div>
```

<h3>Full list of callouts</h3>
<p>The following callout classes/colors are available for use.</p>
<code class="text-white" style="background-color: #84D2E2; display: inline-block; line-height: initial;">callout-info</code> 
<code class="text-white" style="background-color: #F19E1F; display: inline-block; line-height: initial;">callout-warning</code> 
<code class="text-white" style="background-color: #B75527; display: inline-block; line-height: initial;">callout-danger</code> 
<code class="text-white bg-red" style="display: inline-block; line-height: initial;">callout-red</code> 
<code class="text-white bg-blue" style="display: inline-block; line-height: initial;">callout-blue</code> 
<code class="text-white bg-bloom" style="display: inline-block; line-height: initial;">callout-bloom</code> 
<code class="text-white bg-chili" style="display: inline-block; line-height: initial;">callout-chili</code> 
<code class="text-white bg-sky" style="display: inline-block; line-height: initial;">callout-sky</code> 
<code class="text-white bg-midnight" style="display: inline-block; line-height: initial;">callout-midnight</code> 
<code class="text-white bg-oasis" style="display: inline-block; line-height: initial;">callout-oasis</code>
<code class="text-white bg-azurite" style="display: inline-block; line-height: initial;">callout-azurite</code> 
<code class="bg-cool-gray" style="display: inline-block; line-height: initial;">callout-cool-gray</code> 
<code class="bg-warm-gray" style="display: inline-block; line-height: initial;">callout-warm-gray</code>
<code class="text-white bg-leaf" style="display: inline-block; line-height: initial;">callout-leaf</code> 
<code class="text-white bg-river" style="display: inline-block; line-height: initial;">callout-river</code> 
<code class="text-white bg-silver" style="display: inline-block; line-height: initial;">callout-silver</code> 
<code class="text-white bg-mesa" style="display: inline-block; line-height: initial;">callout-mesa</code>

